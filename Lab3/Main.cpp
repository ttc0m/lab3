#include "CryptoHelper.h"
#include "SPCrypto.h"

int main(int argc, char** argv) {
	// mode key out
	if (argc < 6) {
	
		std::cout << "Usage: \n\t" << argv[0] << " <crypt|decrypt> <in_file> <out_file> <key_file> <IV_file>" << std::endl;
		return -1;
	}

	SPCrypto spCrypto = SPCrypto();

	const std::string strCryptMode = "crypt";
	const std::string strDecryptMode = "decrypt";

	std::string argMode(argv[1]);
	std::string argInFile(argv[2]);
	std::string argOutFile(argv[3]);
	std::string argKeyFile(argv[4]);
	std::string argIVFile(argv[5]);

	spCrypto.setKey(argKeyFile);
	spCrypto.setIV(argIVFile);

	if (argMode == strCryptMode) {
	
		spCrypto.getInFlow(argInFile);
		spCrypto.encrypt();
		spCrypto.getOutFlow(argOutFile);
	}
	else if (argMode == strDecryptMode) {
	
		spCrypto.setOutFlow(argInFile);
		spCrypto.decrypt();
		spCrypto.getDecFlow(argOutFile);
	}
	else {
	
		std::cout << "Usage: " << argv[0] << "<crypt|decrypt> <in_file> <out_file> <key_file>";
		return -1;
	}

	return 0;
}