#include "CryptoHelper.h"

CryptoHelper::blocks_t CryptoHelper::splitByBlocks(flow_t inFlow){
	
	blocks_t inBlocks;

	size_t flowLen = inFlow.size() * 8;
	size_t blocksLen = flowLen / block_len;
	size_t elemsInBlock = block_len / (sizeof(flowelem_t) * 8);
	blocksLen += flowLen % block_len ? 1 : 0;

	lastBlockSize = block_len;
	fitNeed = false;
	fitNeed = (bool) (flowLen % block_len);
	
	if (fitNeed) {

		lastBlockSize = flowLen % block_len;
	}

	for (size_t curBlockNum = 0; curBlockNum < blocksLen; curBlockNum++) {
		
		block_t curBlock = 0;
		flow_t::iterator curFlowPos = inFlow.begin() + curBlockNum * elemsInBlock;
		size_t left = inFlow.end() - curFlowPos;
		flow_t::iterator stopFlowPos =  left > elemsInBlock ? /*curFlowPos + (curBlockNum + 1) * elemsInBlock*/ curFlowPos + elemsInBlock : inFlow.end();

		for (curFlowPos; curFlowPos != stopFlowPos; curFlowPos++) {
			curBlock <<= sizeof(flowelem_t) * 8;
			curBlock = curBlock.to_ulong() + *curFlowPos;
		}

		inBlocks.push_back(curBlock);
	}

	return inBlocks;
}

CryptoHelper::sub_blocks_t CryptoHelper::splitBySubBlocks(block_t inBlock){
	auto outSubBlocks = sub_blocks_t();
	auto curSubBlock = sub_block_t();

	for (size_t i = 0; i < sub_block_count; i++) {
	
		for (size_t j = 0; j < m; j++)
			curSubBlock[j] = inBlock[i * m + j];

		outSubBlocks.push_back(curSubBlock);
	}
	

	return outSubBlocks;
}

CryptoHelper::block_t CryptoHelper::catSubBlocks(sub_blocks_t inSubBlocks){
	auto newBlock = block_t();

	for (size_t i = 0; i < sub_block_count; i++) {
	
		for (size_t j = 0; j < m; j++)
			newBlock[i * m + j] = inSubBlocks[i][j];
	}

	return newBlock;
}

CryptoHelper::sub_block_t CryptoHelper::shuffleWithS(sub_block_t inBlock){
	
	auto shuffled = sub_block_t();
	auto curPos = S.begin();
	for (curPos = S.begin(); *curPos != (uint32_t)inBlock.to_ulong(); curPos++);

	if (curPos == S.end() - 1)
		curPos = S.begin();
	else
		curPos++;

	shuffled = *curPos;

	return shuffled;
}

CryptoHelper::sub_block_t CryptoHelper::deshuffleWithS(sub_block_t inBlock) {

	auto shuffled = sub_block_t();
	auto curPos = S.end();
	for (curPos = S.end() - 1; *curPos != (uint32_t)inBlock.to_ulong(); curPos--) {
		
	};

	if (curPos == S.begin())
		curPos = S.end() - 1;
	else
		curPos--;

	shuffled = *curPos;

	return shuffled;
}

