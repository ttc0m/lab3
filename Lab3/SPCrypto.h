#pragma once
#include "CryptoHelper.h"
#include <iostream>
#include <fstream>

class SPCrypto {

	CryptoHelper cHelper;
	CryptoHelper::blocks_t inBlocks;
	CryptoHelper::blocks_t outBlocks;
	CryptoHelper::blocks_t decBlocks;
	CryptoHelper::flow_t inFlow;
	CryptoHelper::flow_t outFlow;
	CryptoHelper::flow_t decFlow;
	CryptoHelper::key_t key;
	CryptoHelper::KS_t vKS;

public:
	SPCrypto();
	void setIV(std::string inFile);
	void setKey(std::string keyFile) {
		uint32_t keyVal;
		std::ifstream keyData(keyFile, std::ios::in | std::ios::binary );
		keyData.read((char*)&keyVal, sizeof(keyVal));

		cHelper.setKey(keyVal);
		cHelper.genKS();
		vKS = cHelper.getKS();
	}

	void setOutFlow(std::string inFile);
	void getInFlow(std::string inFile);
	void getDecFlow(std::string decFile);
	void getOutFlow(std::string outFile);
	
	CryptoHelper::block_t vanishing(CryptoHelper::block_t inBlock) {
	
		return inBlock ^ vKS[cHelper.d - 1];
	}

	void encrypt();
	void decrypt();
	bool validate();

	CryptoHelper::block_t  encryptBlock(CryptoHelper::block_t inBlock) {
		auto outBlock = inBlock;
		for(size_t round = 0; round < cHelper.d; round++)
			outBlock = roundSigma(outBlock, vKS[round]);
		outBlock = vanishing(outBlock);

		return outBlock;
	};

	CryptoHelper::block_t  decryptBlock(CryptoHelper::block_t inBlock) {

		auto outBlock = inBlock;
		outBlock = vanishing(outBlock);
		for (size_t round = 0; round < cHelper.d; round++)
			outBlock = roundDeSigma(outBlock, vKS[round]);

		return outBlock;
	}

private:
	CryptoHelper::block_t roundSigma(CryptoHelper::block_t inBlock, CryptoHelper::key_t roundKey);
	CryptoHelper::block_t roundDeSigma(CryptoHelper::block_t inBlock, CryptoHelper::key_t roundKey);

	CryptoHelper::block_t pShuffle(CryptoHelper::block_t inBlock);

	CryptoHelper::block_t pDeShuffle(CryptoHelper::block_t inBlock);
	CryptoHelper::block_t IV;

};