#pragma once
#include <iostream>
#include <bitset>
#include <algorithm>
#include <cstdlib>
#include <cctype>
#include <vector>

class CryptoHelper {

public:

	static const unsigned word_len = 32;
	static const unsigned key_len = word_len;
	static const unsigned block_len = word_len;
	static const unsigned KSLen = 5;
	static const unsigned d = 4;
	static const unsigned m = 4;
	static const unsigned sub_block_count = block_len / m;
	typedef std::bitset<word_len> word_t;
	typedef word_t block_t;
	typedef std::bitset<m> sub_block_t;
	typedef std::vector<sub_block_t> sub_blocks_t;
	typedef std::vector<block_t> blocks_t;
	typedef std::bitset<key_len> key_t;
	typedef std::vector<key_t> KS_t;
	typedef std::vector<uint32_t> S_t;
	typedef uint8_t flowelem_t;
	typedef std::vector<flowelem_t> flow_t;

private:
	key_t key;
	KS_t vKS;

	S_t S = {
		10, 0, 9, 14,
		6, 3, 15, 5,
		1, 13, 12, 7,
		11, 4, 2, 8
	};

	bool fitNeed;
	size_t lastBlockSize;

public:
	void setKey(uint32_t keyVal) { key = keyVal; };
	key_t get_key() { return key; };
	void genKS() { 
		vKS.clear();
		for (unsigned i = 0; i < KSLen; i++)
			vKS.push_back(key);
	};

	KS_t getKS() { return vKS; }

	unsigned P(unsigned bitPos) {
	
		return (29 * bitPos + 31) % 32;
	}

	unsigned deP(unsigned bitPos) {

		return (21 * (bitPos + 1)) % 32;
	}

	blocks_t splitByBlocks(flow_t inFlow);
	sub_blocks_t splitBySubBlocks(block_t inBlock);
	block_t catSubBlocks(sub_blocks_t inSubBlocks);
	sub_block_t shuffleWithS(sub_block_t inBlock);
	CryptoHelper::sub_block_t deshuffleWithS(sub_block_t inBlock);
	bool isFitNeed() { return fitNeed; }
	size_t getLastBlockLen() { return lastBlockSize; }
};